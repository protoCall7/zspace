const std = @import("std");

const Astros = struct {
    message: []u8,
    number: i64,
    people: []Person,
};

const Person = struct {
    name: []u8,
    craft: []u8,
};

pub fn main() !void {
    // set up the memory allocator
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer _ = gpa.deinit();

    // set up the HTTP client
    var client: std.http.Client = .{ .allocator = allocator };
    defer client.deinit();

    // parse the URI
    const uri = try std.Uri.parse("http://api.open-notify.org/astros.json");

    // set up the request headers
    var headers: std.http.Headers = .{ .allocator = allocator };
    defer headers.deinit();
    try headers.append("Accept", "application/json");

    // set up the HTTP request
    var request = try client.request(.GET, uri, headers, .{});
    defer request.deinit();

    // begin the request and wait for the response from the server
    try request.start();
    try request.wait();

    if (request.response.status == .ok) {
        // read the body data in
        // allocation is based on the content-length header
        const data = try request.reader().readAllAlloc(allocator, request.response.content_length.?);
        defer allocator.free(data);

        // parse the JSON
        const astros = try std.json.parseFromSlice(Astros, allocator, data, .{});
        defer std.json.parseFree(Astros, allocator, astros);

        // output
        std.debug.print("There are {d} people in space:\n", .{astros.number});
        for (astros.people) |p| {
            std.debug.print("{s} is aboard {s}\n", .{ p.name, p.craft });
        }
    }
}
